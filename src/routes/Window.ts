export class Window {
	x: number;
	y: number;
	width: number;
	height: number;
	content: any;
	label?: string;
	children: Window[] = []

	constructor(x: number, y: number, width: number, height: number, content: any, label?: string) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height= height;
		this.content = content;
		this.label = label;
	}

	draw(parent: any) {
		return {x: this.x, y: this.y, width: this.width, height: this.height}
	}
}