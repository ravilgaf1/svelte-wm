import { Window } from "./Window";

export class WindowManager {
	container: any;
	windows: Window[] = [];
	// containerSize: {x: number, y: number} = {x: window.innerWidth - 3, y: window.innerHeight - 3};

	constructor(container: any) {
		this.container = container;

		// this.container.style.width = this.containerSize.x + "px";
		// this.container.style.height = this.containerSize.y + "px";
	}

	addWindow(content: any, label: string) {
		let countWindow = this.windows.length + 1;
		let numColumns = Math.ceil(Math.sqrt(countWindow));
		let numRows = Math.ceil(countWindow / numColumns);
		
		let width = (this.container.offsetWidth - (numColumns + 1) * 5) / numColumns;
		let height = (this.container.offsetHeight - (numRows + 1) * 5) / numRows;

		// Изменение существующих окон
		this.windows.forEach((window, i) => {
			//
		})

		let newWindowX = this.container.offsetLeft + ((countWindow - 1) % numColumns) * (width + 5) + 5;
		let newWindowY = this.container.offsetTop + Math.floor((countWindow - 1) / numColumns) * (height + 5) + 5;

		let newWindow = new Window(newWindowX, newWindowY, width, height, content, label);

		this.windows.push(newWindow);

		console.log(this.windows);
		
	}
}